/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sbuf.h"
#include <rsys/math.h>
#include <rsys/mem_allocator.h>
#include <string.h>

struct type_desc {
  void (*set)(void* data, const size_t i);
  int (*eq)(const void* a, const void* b);

  uint64_t size;
  uint64_t alignment;
};

struct header {
  uint64_t pagesize;
  uint64_t size;
  uint64_t szitem;
  uint64_t alitem;
};

/*******************************************************************************
 * i16_f32 type
 ******************************************************************************/
struct i16_f32 {
  int16_t i16;
  float f32;
};

static void
i16_f32_set(void* data, const size_t i)
{
  struct i16_f32* t = data;
  CHK(t);
  t->i16 = (int16_t)i;
  t->f32 = (float)i/100.f;
}

static int
i16_f32_eq(const void* a, const void* b)
{
  const struct i16_f32* t0 = a;
  const struct i16_f32* t1 = b;
  CHK(t0 && t1);
  return t0->i16 == t1->i16 && t0->f32 == t1->f32;
}

/*******************************************************************************
 * char[7]
 ******************************************************************************/
typedef char (char7_T) [7];

static void
char7_set(void* data, const size_t i)
{
  char* str = data;
  CHK(i < 1000);
  sprintf(str, "%3dabc", (int)i);
}

static int
char7_eq(const void* a, const void* b)
{
  const char* s0 = a;
  const char* s1 = b;
  CHK(s0 && s1);
  return strcmp(s0, s1) == 0;
}

/*******************************************************************************
 * ui32_f32 type
 ******************************************************************************/
struct ui32_f32 {
  uint32_t ui32;
  float f32;
};

static void
ui32_f32_set(void* data, const size_t i)
{
  struct ui32_f32* t = data;
  CHK(t);
  t->ui32 = (uint32_t)i;
  t->f32 = (float)i/100.f;
}

static int
ui32_f32_eq(const void* a, const void* b)
{
  const struct ui32_f32* t0 = a;
  const struct ui32_f32* t1 = b;
  CHK(t0 && t1);
  return t0->ui32 == t1->ui32 && t0->f32 == t1->f32;
}

/*******************************************************************************
 * float
 ******************************************************************************/
static void
f32_set(void* data, const size_t i)
{
  float* f = data;
  CHK(f);
  *f = (float)i;
}

static int
f32_eq(const void* a, const void* b)
{
  const float* f0 = a;
  const float* f1 = b;
  CHK(f0 && f1);
  return *f0 == *f1;
}

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
check_sbuf_desc
  (const struct sbuf_desc* desc,
   const uint64_t size,
   const struct type_desc* type)
{
  char ALIGN(512) mem[512] = {0};
  size_t i;
  CHK(desc && type);
  CHK(type->size <= sizeof(mem));
  CHK(type->alignment <= ALIGNOF(mem));

  CHK(desc->buffer != NULL);
  CHK(desc->size == size);
  CHK(desc->szitem == type->size);
  CHK(desc->alitem == type->alignment);
  CHK(desc->pitch == ALIGN_SIZE(type->size, type->alignment));

  FOR_EACH(i, 0, size) {
    const void* item = sbuf_desc_at(desc, i);
    CHK(IS_ALIGNED(item, desc->alitem));
    type->set(mem, i);
    CHK(type->eq(mem, item));
  }
}

static void
write_buffer
  (FILE* fp,
   const struct header* header,
   const struct type_desc* type)
{
  char ALIGN(512) mem[512] = {0};
  size_t i;
  const char byte = 0;

  CHK(type);
  CHK(type->size <= sizeof(mem));

  /* Write file header */
  CHK(fwrite(&header->pagesize, sizeof(header->pagesize), 1, fp) == 1);
  CHK(fwrite(&header->size, sizeof(header->size), 1, fp) == 1);
  CHK(fwrite(&header->szitem, sizeof(header->szitem), 1, fp) == 1);
  CHK(fwrite(&header->alitem, sizeof(header->alitem), 1, fp) == 1);

  /* Padding */
  CHK(fseek(fp,
    (long)ALIGN_SIZE((size_t)ftell(fp), header->pagesize), SEEK_SET) == 0);

  /* Write the buffer data */
  FOR_EACH(i, 0, header->size) {
    type->set(mem, i);
    CHK(fwrite(mem, type->size, 1, fp) == 1);
    CHK(fseek(fp,
      (long)ALIGN_SIZE((size_t)ftell(fp), type->alignment), SEEK_SET) == 0);
  }

  /* Padding. Write one char to position the EOF indicator */
  CHK(fseek(fp,
    (long)ALIGN_SIZE((size_t)ftell(fp), header->pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);
  CHK(fflush(fp) == 0);
}

static void
test_misc(struct sbuf* buf)
{
  struct sbuf_desc desc = SBUF_DESC_NULL;
  struct header header;
  struct type_desc type;
  FILE* fp = NULL;
  CHK(buf);

  type.set = f32_set;
  type.eq = f32_eq;
  type.size = sizeof(float);
  type.alignment = ALIGNOF(float);

  header.pagesize = 16384;
  header.size = 123;
  header.szitem = type.size;
  header.alitem = type.alignment;

  CHK(fp = tmpfile());
  write_buffer(fp, &header, &type);
  rewind(fp);

  CHK(sbuf_load_stream(NULL, fp, NULL) == RES_BAD_ARG);
  CHK(sbuf_load_stream(buf, NULL, NULL) == RES_BAD_ARG);
  CHK(sbuf_load_stream(buf, fp, NULL) == RES_OK);

  rewind(fp);
  CHK(sbuf_load_stream(buf, fp, "<stream>") == RES_OK);

  CHK(sbuf_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(sbuf_get_desc(buf, NULL) == RES_BAD_ARG);
  CHK(sbuf_get_desc(buf, &desc) == RES_OK);
  check_sbuf_desc(&desc, header.size, &type);

  CHK(fclose(fp) == 0);
}

static void
test_buffer
  (struct sbuf* buf,
   const struct header* header,
   const struct type_desc* type,
   const res_T res)
{
  FILE* fp = NULL;
  CHK(buf && type);

  CHK(fp = tmpfile());
  write_buffer(fp, header, type);
  rewind(fp);

  CHK(sbuf_load_stream(buf, fp, NULL) == res);

  if(res == RES_OK) {
    struct sbuf_desc desc = SBUF_DESC_NULL;
    CHK(sbuf_get_desc(buf, &desc) == RES_OK);
    check_sbuf_desc(&desc, header->size, type);
  }

  CHK(fclose(fp) == 0);
}

static void
test_load(struct sbuf* buf)
{
  struct header header;
  struct type_desc type;

  type.set = i16_f32_set;
  type.eq = i16_f32_eq;
  type.size = sizeof(struct i16_f32);
  type.alignment = ALIGNOF(struct i16_f32);

  header.pagesize = 16384;
  header.size = 287;
  header.szitem = type.size;
  header.alitem = type.alignment;

  test_buffer(buf, &header, &type, RES_OK);

  type.alignment = header.alitem = 32;
  test_buffer(buf, &header, &type, RES_OK);

  type.set = char7_set;
  type.eq = char7_eq;
  type.size = header.szitem = sizeof(char7_T);
  type.alignment = header.alitem = ALIGNOF(char7_T);
  test_buffer(buf, &header, &type, RES_OK);

  type.set = ui32_f32_set;
  type.eq = ui32_f32_eq;
  type.size = header.szitem = sizeof(struct ui32_f32);
  type.alignment = header.alitem = ALIGNOF(struct ui32_f32);
  test_buffer(buf, &header, &type, RES_OK);

  type.set = f32_set;
  type.eq = f32_eq;
  type.size = header.szitem = sizeof(float);
  type.alignment = header.alitem = ALIGNOF(float);
  test_buffer(buf, &header, &type, RES_OK);
}

static void
test_load_fail(struct sbuf* buf)
{
  struct type_desc type;
  struct header header;
  uint64_t size = 0;
  FILE* fp = NULL;

  type.set = f32_set;
  type.eq = f32_eq;
  type.size = sizeof(float);
  type.alignment = 32;

  header.pagesize = 4096;
  header.size = 100;
  header.szitem = type.size;
  header.alitem = type.alignment;

  /* Check the a priori validity of the current parameters */
  test_buffer(buf, &header, &type, RES_OK);

  /* Invalid page size */
  header.pagesize = 2048;
  test_buffer(buf, &header, &type, RES_BAD_ARG);
  header.pagesize = 4098;
  test_buffer(buf, &header, &type, RES_BAD_ARG);

  /* Invalid size */
  header.pagesize = 4096;
  header.size = 0;
  test_buffer(buf, &header, &type, RES_BAD_ARG);

  /* Invalid item  size */
  header.size = 100;
  header.szitem = 0;
  test_buffer(buf, &header, &type, RES_BAD_ARG);

  /* Invalid type alignment */
  header.szitem = type.size;
  header.alitem = 0;
  test_buffer(buf, &header, &type, RES_BAD_ARG);
  header.alitem = 33;
  test_buffer(buf, &header, &type, RES_BAD_ARG);
  header.alitem = 8192;
  test_buffer(buf, &header, &type, RES_BAD_ARG);

  /* Invalid file size */
  header.alitem = type.alignment;
  CHK(fp = tmpfile());
  write_buffer(fp, &header, &type);
  CHK(fseek(fp, 8, SEEK_SET) == 0); /* Overwrite the size */
  size = 5000;
  CHK(fwrite(&size, sizeof(size), 1, fp) == 1);
  rewind(fp);
  CHK(sbuf_load_stream(buf, fp, NULL) == RES_IO_ERR);
  CHK(fclose(fp) == 0);
}

static void
test_load_files(struct sbuf* buf, int argc, char** argv)
{
  int i;
  CHK(buf);
  FOR_EACH(i, 1, argc) {
    struct sbuf_desc desc = SBUF_DESC_NULL;
    size_t iitem;

    printf("Loading %s\n", argv[i]);
    CHK(sbuf_load(buf, argv[i]) == RES_OK);
    CHK(sbuf_get_desc(buf, &desc) == RES_OK);
    CHK(desc.buffer);
    CHK(desc.size);
    CHK(desc.szitem);
    CHK(desc.alitem);
    CHK(desc.pitch);
    CHK(IS_POW2(desc.alitem));

    FOR_EACH(iitem, 0, desc.size) {
      const void* data = NULL;
      data = sbuf_desc_at(&desc, iitem);
      CHK(data != 0);
      CHK(IS_ALIGNED(data, desc.alitem));
      if(iitem) {
        const void* prev = (char*)data - desc.pitch;
        CHK(prev == sbuf_desc_at(&desc, iitem-1));
      }
    }
  }
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sbuf_create_args args = SBUF_CREATE_ARGS_DEFAULT;
  struct sbuf* buf = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(sbuf_create(&args, &buf) == RES_OK);

  if(argc > 1) {
    test_load_files(buf, argc, argv);
  } else {
    test_misc(buf);
    test_load(buf);
    test_load_fail(buf);
  }

  CHK(sbuf_ref_put(buf) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}

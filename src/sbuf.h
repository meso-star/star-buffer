/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SBUF_H
#define SBUF_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SBUF_SHARED_BUILD) /* Build shared library */
  #define SBUF_API extern EXPORT_SYM
#elif defined(SBUF_STATIC) /* Use/build static library */
  #define SBUF_API extern LOCAL_SYM
#else /* Use shared library */
  #define SBUF_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the smsh function `Func'
 * returns an error. One should use this macro on smsh function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define SBUF(Func) ASSERT(sbuf_ ## Func == RES_OK)
#else
  #define SBUF(Func) sbuf_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

struct sbuf_create_args {
  struct logger* logger; /* May be NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define SBUF_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct sbuf_create_args SBUF_CREATE_ARGS_DEFAULT =
  SBUF_CREATE_ARGS_DEFAULT__;

struct sbuf_desc {
  const void* buffer;
  size_t size; /* #items in the buffer */
  size_t szitem; /* Size of a buffer item */
  size_t alitem; /* Alignment of a buffer item */
  size_t pitch; /* #bytes between 2 consecutive items */
};
#define SBUF_DESC_NULL__ {NULL, 0, 0, 0, 0}
static const struct sbuf_desc SBUF_DESC_NULL = SBUF_DESC_NULL__;

/* Forward declaration of opaque data types */
struct sbuf;

BEGIN_DECLS

/*******************************************************************************
 * Star-Buffer API
 ******************************************************************************/
SBUF_API res_T
sbuf_create
  (const struct sbuf_create_args* args,
   struct sbuf** sbuf);

SBUF_API res_T
sbuf_ref_get
  (struct sbuf* sbuf);

SBUF_API res_T
sbuf_ref_put
  (struct sbuf* sbuf);

SBUF_API res_T
sbuf_load
  (struct sbuf* sbuf,
   const char* path);

SBUF_API res_T
sbuf_load_stream
  (struct sbuf* sbuf,
   FILE* stream,
   const char* stream_name); /* NULL <=> use default stream name */

SBUF_API res_T
sbuf_get_desc
  (const struct sbuf* sbuf,
   struct sbuf_desc* desc);

static INLINE const void*
sbuf_desc_at
  (const struct sbuf_desc* desc,
   const size_t iitem)
{
  ASSERT(desc && iitem < desc->size);
  return (char*)desc->buffer + iitem*desc->pitch;
}

END_DECLS

#endif /* SBUF_H */

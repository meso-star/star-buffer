/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "sbuf.h"
#include "sbuf_c.h"
#include "sbuf_log.h"

#include <rsys/mem_allocator.h>

#include <errno.h>
#include <unistd.h>
#include <sys/mman.h> /* mmap */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_sbuf_create_args(const struct sbuf_create_args* args)
{
  /* Nothing to check. Only return RES_BAD_ARG if args is NULL */
  return args ? RES_OK : RES_BAD_ARG;
}

static void
reset_sbuf(struct sbuf* sbuf)
{
  ASSERT(sbuf);
  sbuf->pagesize = 0;
  sbuf->size = 0;
  sbuf->szitem = 0;
  sbuf->alitem = 0;
  if(sbuf->buffer && sbuf->buffer != MAP_FAILED)
    munmap(sbuf->buffer, sbuf->map_len);
  sbuf->buffer = NULL;
  sbuf->map_len= 0;
}

static res_T
map_data
  (struct sbuf* sbuf,
   const char* stream_name,
   const int fd, /* File descriptor */
   const size_t filesz, /* Overall filesize */
   const off_t offset, /* Offset of the data into file */
   const size_t map_len,
   void** out_map) /* Lenght of the data to map */
{
  void* map = NULL;
  res_T res = RES_OK;
  ASSERT(sbuf && stream_name && filesz && map_len && out_map);
  ASSERT(IS_ALIGNED((size_t)offset, (size_t)sbuf->pagesize));

  if((size_t)offset + map_len > filesz) {
    log_err(sbuf, "%s: the amount of data to load exceed the file size.\n",
      stream_name);
    res = RES_IO_ERR;
    goto error;
  }

  map = mmap(NULL, map_len, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fd, offset);
  if(map == MAP_FAILED) {
    log_err(sbuf, "%s: could not map the data -- %s.\n",
      stream_name, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

exit:
  *out_map = map;
  return res;
error:
  if(map == MAP_FAILED) map = NULL;
  goto exit;
}

static res_T
load_stream(struct sbuf* sbuf, FILE* stream, const char* stream_name)
{
  off_t offset;
  size_t filesz;
  res_T res = RES_OK;
  ASSERT(sbuf && stream && stream_name);

  reset_sbuf(sbuf);

  /* Read file header */
  #define READ(Var, N, Name) {                                                 \
    if(fread((Var), sizeof(*(Var)), (N), stream) != (N)) {                     \
      log_err(sbuf, "%s: could not read the %s.\n", stream_name, (Name));      \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&sbuf->pagesize, 1, "page size");
  READ(&sbuf->size, 1, "number of items");
  READ(&sbuf->szitem, 1, "size of an item");
  READ(&sbuf->alitem, 1, "alignment of an item");
  #undef READ

  if(!IS_ALIGNED(sbuf->pagesize, sbuf->pagesize_os)) {
    log_err(sbuf,
      "%s: invalid page size %li. The page size attribute must be aligned on "
      "the page size of the operating system (%lu).\n",
      stream_name, sbuf->pagesize, (unsigned long)sbuf->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!sbuf->size) {
    log_err(sbuf,
      "%s: invalid buffer size %lu. "
      "The number of items stored cannot be zero.\n",
      stream_name, sbuf->size);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!sbuf->szitem) {
    log_err(sbuf,
      "%s: invalid item size `%lu'.\n",
      stream_name, (unsigned long)sbuf->szitem);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!IS_POW2(sbuf->alitem)) {
    log_err(sbuf,
      "%s: invalid item alignment `%lu'. It must be a power of 2.\n",
      stream_name, (unsigned long)sbuf->alitem);
    res = RES_BAD_ARG;
    goto error;
  }
  if(sbuf->alitem > sbuf->pagesize) {
    log_err(sbuf,
      "%s: invalid item alignment `%lu'. "
      "It must be less than the provided pagesize `%lu'.\n",
      stream_name, (unsigned long)sbuf->alitem, (unsigned long)sbuf->pagesize);
    res = RES_BAD_ARG;
    goto error;

  }

  sbuf->pitch = ALIGN_SIZE(sbuf->szitem, sbuf->alitem);

  /* Compute the length in bytes of the data to map */
  sbuf->map_len = sbuf->size * sbuf->pitch;
  sbuf->map_len = ALIGN_SIZE(sbuf->map_len, (size_t)sbuf->pagesize);

  /* Find the offsets of the data into the stream */
  offset = (off_t)ALIGN_SIZE((uint64_t)ftell(stream), sbuf->pagesize);

  /* Retrieve the overall filesize */
  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  /* Map the data */
  res = map_data(sbuf, stream_name, fileno(stream), filesz, offset,
    sbuf->map_len, &sbuf->buffer);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  reset_sbuf(sbuf);
  goto exit;
}


static void
release_sbuf(ref_T* ref)
{
  struct sbuf* sbuf;
  ASSERT(ref);
  sbuf = CONTAINER_OF(ref, struct sbuf, ref);
  reset_sbuf(sbuf);
  if(sbuf->logger == &sbuf->logger__) logger_release(&sbuf->logger__);
  MEM_RM(sbuf->allocator, sbuf);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sbuf_create
  (const struct sbuf_create_args* args,
   struct sbuf** out_sbuf)
{
  struct sbuf* sbuf = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_sbuf) { res = RES_BAD_ARG; goto error; }
  res = check_sbuf_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  sbuf = MEM_CALLOC(allocator, 1, sizeof(*sbuf));
  if(!sbuf) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the Star-Buffer device.\n"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&sbuf->ref);
  sbuf->allocator = allocator;
  sbuf->verbose = args->verbose;
  sbuf->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  if(args->logger) {
    sbuf->logger = args->logger;
  } else {
    setup_log_default(sbuf);
  }

exit:
  if(out_sbuf) *out_sbuf = sbuf;
  return res;
error:
  if(sbuf) { SBUF(ref_put(sbuf)); sbuf = NULL; }
  goto exit;
}

res_T
sbuf_ref_get(struct sbuf* sbuf)
{
  if(!sbuf) return RES_BAD_ARG;
  ref_get(&sbuf->ref);
  return RES_OK;
}

res_T
sbuf_ref_put(struct sbuf* sbuf)
{
  if(!sbuf) return RES_BAD_ARG;
  ref_put(&sbuf->ref, release_sbuf);
  return RES_OK;
}


res_T
sbuf_load(struct sbuf* sbuf, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!sbuf || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(sbuf, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(sbuf, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
sbuf_load_stream
  (struct sbuf* sbuf,
   FILE* stream,
   const char* stream_name)
{
  if(!sbuf || !stream) return RES_BAD_ARG;
  return load_stream(sbuf, stream, stream_name ? stream_name : "<stream>");
}

res_T
sbuf_get_desc(const struct sbuf* sbuf, struct sbuf_desc* desc)
{
  if(!sbuf || !desc) return RES_BAD_ARG;
  desc->buffer = sbuf->buffer;
  desc->size = sbuf->size;
  desc->szitem = (size_t)sbuf->szitem;
  desc->alitem = (size_t)sbuf->alitem;
  desc->pitch = (size_t)sbuf->pitch;
  return RES_OK;
}
